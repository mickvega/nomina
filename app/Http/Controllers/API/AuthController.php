<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Property;
use App\PasswordResets;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


use App\Mail\VerificationCode;
use App\Mail\ResetPassword;
use App\Mail\Invite;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PDF;
class AuthController extends Controller
{
  /**
  * Handle an authentication attempt.
  *
  * @return Response
  */
  public function login(Request $request)
  {
      $validatedData = $request->validate([
        'email' => 'required|string|email|max:255',
        'password' => 'required|string|min:6',
      ]);
     if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) 
     {

        $user = Auth::user();
         //load picture
        $picture='';
        if(isset($user['avatar']) && $user['avatar'] != '')
        {
          
          if(Storage::has('public/'.$user['avatar']))
          {  
            $avatar = Storage::get('public/'.$user['avatar']);
            $picture = base64_encode($avatar);
          }
        }
        // Authentication passed...
         //returns the logged user
         
         $success['token'] = $user->createToken('Token')->accessToken;
         //devuelve un json con los datos del ingresado y el token.
         return response()->json(['user' => $user, 'token' => $success['token'], 'picture' => $picture], 200);
     } else {
       //401 es el código http de no autorizado por inicio de sesión.
       return response()->json(['error'=>'Las credenciales no coinciden'], 401);
     }
  }


  public function profile() {
    $user = Auth::user();
    $hash=md5($user['id'].'f1nt3ch'.$user['email']);
    return response()->json(['success' => $user, 'hash'=> $hash], 200);
  }
  public function test()
  {
    $property = Property::where('id', '=','7')->with('image_name')->get();
    /*$pdf = PDF::loadView('about', ['properties' => $property[0]  ]);
    return $pdf->download('invoice.pdf');*/
    return view('about', ['properties' => $property[0]  ]);
  }



  /**
  * Handle an authentication attempt.
  *
  * @return Void
  */


}
