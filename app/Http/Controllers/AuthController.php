<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class AuthController extends Controller
{
  /**
  * Handle an authentication attempt.
  *
  * @return Response
  */

  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required',
        'password_confirmation' => 'required|same:password',
    ]);
    if($validator->fails())
      return response()->json(['status' => 'error', 'error' =>  $validator->errors()], 200);
    
    $input = $request->all();
    $input['password'] = bcrypt($input['password']);

    $user = User::create($input);
    $success['token'] =  $user->createToken('nomina')->accessToken;
    $success['name'] =  $user->name;

    return response()->json(['status' => 'success', 'message' =>  'user created'], 200);
    }

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $token =  $user->createToken('nomina')-> accessToken; 
            return response()->json(['status' => 'success', 'user' => $user, 'token' => $token], 200);
        } 
        else{ 
          return response()->json(['status' => 'success', 'message' =>  'las credenciales no coinciden'], 200);
        } 
    }
  /**
  * Handle an authentication attempt.
  *
  * @return Void
  */


}
