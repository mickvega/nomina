<?php

namespace App\Http\Controllers;
use App\Models\Empleado;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmpleadoController extends Controller
{
	public function index()
	{
		$empleados = Empleado::where("deleted", "=", '0')->get();
        return response()->json(['status' => 'success', 'empleados' =>  $empleados], 200);
	}
    public function store(Request $request)
    {
        $data = $request->json()->all();
        $validator = Validator::make($data,Empleado::rules());
        if($validator->fails())
          return response()->json(['status' => 'error', 'error' =>  $validator->errors()], 200);

        $empleado = Empleado::create($data);
        if($empleado)
            return response()->json(['status' => 'success', 'empleado' =>  $empleado], 200);
        else
            return response()->json(['status' => 'error', 'message' =>  'error'], 200);
    }

    public function update(Request $request,$id)
    {
        $data = $request->json()->all();
        $data['id'] = $id;
        $validator = Validator::make($data, Empleado::update_rules());
        if($validator->fails())
          return response()->json(['status' => 'error', 'error' =>  $validator->errors()], 200);

        $empleado = Empleado::find($id);
        $empleado->update($data);
        return response()->json(['status' => 'success', 'message' => 'empleado updated'], 200);
    }

    public function destroy($id)
    {
        $data['id'] = $id;
        $validator = Validator::make($data, ["id" => "required|exists:empleados,id"]);
        if($validator->fails())
          return response()->json(['status' => 'error', 'error' =>  $validator->errors()], 200);

        $empleado = Empleado::find($id);
        $empleado->deleted = 1;
        $empleado->save();

        return response()->json(['status' => 'success', 'message' => 'empleado deleted'], 200);
    }
}
