<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'last_name','email', 'code', 'second_name', 'contract_type', 'state', 'deleted'];

    public static function rules()
    {
        return [ 
            "id" => "required|exists:empleado,id",
            "name" => "required|string",
            "email" => "required|email",
            "code" => "required|string",
            "last_name" => "required|string",
            "second_name" => "required|string",
            "contract_type" => "required|integer",
            "state" => 'required|integer',
            "deleted" => "required"
        ];
    }

    public static function update_rules()
    {
    	return [ 
    		"name" => "required|string",
    		"email" => "required|email",
    		"code" => "required|string",
    		"last_name" => "required|string",
    		"second_name" => "required|string",
    		"contract_type" => "required|integer",
    		"state" => 'required|integer',
    		"deleted" => "required"
    	];
    }
}
