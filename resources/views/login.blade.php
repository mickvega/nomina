<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </head>
    <body >
        <div class="container ">
            <br>
            <br>
            <div class="row auto">
                <div class="col"></div>
                <div class="col-8 " >
                    <form onsubmit="login()">
                        <label>correo</label>
                        <input class="form-control" type="email" id="email" name="" required="">
                        <br>
                        <label>contraseña</label>
                        <input class="form-control" type="password" id="password" name="" required="">
                        <br>
                        <button class="btn btn-secondary" type="submit" style="">send</button>
                    </form>
                    
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        function login()
        {
            if($('#email').val() != '' && $("#password").val() != '')
            {


                $.ajax({
                url: "http://localhost:8001/api/login",
                   type: "POST",
                   data:  new FormData(this),
                   contentType: false,
                         cache: false,
                   processData:false,
                   beforeSend : function()
                   {
                    //$("#preview").fadeOut();
                    $("#err").fadeOut();
                   },
                   success: function(data)
                      {
                        data = JSON.parse(data);
                        console.log(data);
                      },
                     error: function(e) 
                      {
                    $("#err").html(e).fadeIn();
                    loader('');
                      }          
                    });
            }
            
        }
    </script>
</html>
